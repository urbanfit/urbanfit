# UrbanFit - L'app qui vous fera ressortir.

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# generate static project
$ npm run generate

# launch server
$ npm run start
```
